package fr.itakademy;

import java.util.Scanner;

public class Joueur {
    private String name;
    private Choice choice;
    private int score;

    public Joueur(String name) {
        this.name = name;
        this.score = 0;
    }

    public String getName() {
        return this.name;
    }

    public int getScore() {
        return this.score;
    }

    public void setChoice() {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        if (n < 3 && n >= 0) {
            this.choice = new Choice(n);
        }

    }

    public Choice getChoice() {
        return this.choice;
    }

    public void updateScore() {
        this.score += 1;
    }
}
