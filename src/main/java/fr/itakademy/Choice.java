package fr.itakademy;

public class Choice {
    private String[] possibilities = { "rock", "paper", "scissor" };
    private int value;

    public Choice(int n) {
        this.value = n;
    }

    public int getValue() {
        return this.value;
    }

    public String getChoice() {
        return this.possibilities[this.value];
    }
}
