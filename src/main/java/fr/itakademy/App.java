package fr.itakademy;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Joueur[] joueurs = new Joueur[2];
        joueurs[0] = new Joueur("joueur1");
        joueurs[1] = new Joueur("joueur2");
        Partie partie = new Partie(joueurs, 3);
        partie.game();
    }
}
