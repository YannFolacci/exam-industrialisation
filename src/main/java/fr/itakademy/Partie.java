package fr.itakademy;

import java.io.*;

public class Partie {
    private Joueur[] joueurs = new Joueur[2];
    private int maxRound;
    private int currentRound;

    public Partie(Joueur[] joueurs, int round) {
        this.joueurs = joueurs;
        this.maxRound = round;
    }

    public void game() {
        for (; this.currentRound < this.maxRound; this.currentRound++) {
            this.round();
        }
    }

    private void round() {
        this.playerChoose();
        this.compareChoices();
        System.out.println(this.showScore());
    }

    public void playerChoose() {
        for (Joueur joueur : joueurs) {
            joueur.setChoice();
        }
    }

    public Joueur compareChoices() {
        int choice1 = this.joueurs[0].getChoice().getValue();
        int choice2 = this.joueurs[1].getChoice().getValue();
        if (choice1 == choice2 + 1 || (choice1 == 0 && choice2 == 3)) {
            this.joueurs[0].updateScore();
            return this.joueurs[0];
        }
        if (choice1 != choice2) {
            this.joueurs[1].updateScore();
            return this.joueurs[1];
        }
        return null;
    }

    public String showScore() {
        String s = "";
        for (Joueur joueur : joueurs) {
            s += joueur.getName() + " : " + joueur.getScore() + "\n";
        }
        return s;
    }
}
