package fr.itakademy;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class JoueurTest {
    private InputStream sysInBackup = System.in;

    private void simulateInput(String input) {
        sysInBackup = System.in;
        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
    }

    private Joueur player0;
    private Joueur player1;

    @BeforeEach
    public void init() {
        System.setIn(sysInBackup);
        this.player0 = new Joueur("Toto");
        this.player1 = new Joueur("Tata");

    }

    @Test
    void should_be_paper() {
        this.simulateInput("0");
        this.player0.setChoice();
        assertEquals(0, this.player0.getChoice().getValue());
    }

    void should_update_score() {
        this.player0.updateScore();
        assertEquals(1, this.player0.getScore());
    }
}
