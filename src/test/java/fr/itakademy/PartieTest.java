package fr.itakademy;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PartieTest {

    private InputStream sysInBackup = System.in;

    private void simulateInput(String input) {
        String inputString = "";
        inputString += input;
        sysInBackup = System.in;
        ByteArrayInputStream in = new ByteArrayInputStream(inputString.getBytes());
        System.setIn(in);
    }

    private Partie partie;
    private Joueur[] joueurs;
    private Joueur player0;
    private Joueur player1;

    @BeforeEach
    public void init() {
        System.setIn(sysInBackup);
        this.player0 = new Joueur("Toto");
        this.player1 = new Joueur("Tata");
        this.joueurs = new Joueur[] { this.player0, this.player1 };
        this.partie = new Partie(joueurs, 3);

    }

    @Test
    void compare_choices() {
        simulateInput("0");
        this.player0.setChoice();
        simulateInput("1");
        this.player1.setChoice();
        assertEquals(this.player1, this.partie.compareChoices());
    }

    @Test
    void show_score() {
        this.player1.updateScore();
        assertEquals("Toto : 0\nTata : 1\n", this.partie.showScore());
    }

}
